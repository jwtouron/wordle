{-# LANGUAGE FlexibleInstances, MultiParamTypeClasses, TemplateHaskell #-}

module Main where

import           Data.Foldable
import           Data.Maybe
import           Data.Set (Set)
import qualified Data.Set as Set
import           Lens.Micro.Internal
import           Lens.Micro.Platform

data FiveOf a
  = FiveOf a a a a a
  deriving (Show)

instance Functor FiveOf where
  fmap f (FiveOf a b c d e) = FiveOf (f a) (f b) (f c) (f d) (f e)

instance Applicative FiveOf where
  pure x = FiveOf x x x x x
  FiveOf f1 f2 f3 f4 f5 <*> FiveOf x1 x2 x3 x4 x5 = FiveOf (f1 x1) (f2 x2) (f3 x3) (f4 x4) (f5 x5)

instance Foldable FiveOf where
  foldr f s (FiveOf a1 a2 a3 a4 a5) = foldr f s [a1, a2, a3, a4, a5]

instance Field1 (FiveOf a) (FiveOf a) a a where
  _1 f (FiveOf a b c d e) = f a <&> \a' -> FiveOf a' b c d e
  {-# INLINE _1 #-}

instance Field2 (FiveOf a) (FiveOf a) a a where
  _2 f (FiveOf a b c d e) = f b <&> \b' -> FiveOf a b' c d e
  {-# INLINE _2 #-}

instance Field3 (FiveOf a) (FiveOf a) a a where
  _3 f (FiveOf a b c d e) = f c <&> \c' -> FiveOf a b c' d e
  {-# INLINE _3 #-}

instance Field4 (FiveOf a) (FiveOf a) a a where
  _4 f (FiveOf a b c d e) = f d <&> \d' -> FiveOf a b c d' e
  {-# INLINE _4 #-}

instance Field5 (FiveOf a) (FiveOf a) a a where
  _5 f (FiveOf a b c d e) = f e <&> \e' -> FiveOf a b c d e'
  {-# INLINE _5 #-}

fiveOfFromList :: [a] -> Maybe (FiveOf a)
fiveOfFromList [a, b, c, d, e] = Just (FiveOf a b c d e)
fiveOfFromList _ = Nothing

data State
  = State { _posses :: (FiveOf (Set Char)), _reqs :: (Set Char) }
  deriving (Show)

makeLenses ''State

data Update
  = RightPlace Int Char
  | WrongPlace Int Char

alphabet :: [Char]
alphabet = "abcdefghijklmnopqrstuvwxyz"

newState :: State
newState = State (fromJust $ fiveOfFromList (replicate 5 (Set.fromList alphabet))) Set.empty

updateState :: State -> Update -> State
updateState (State (FiveOf poss1 _ _ _ _) reqs) (RightPlace 0 c) = undefined

matches :: State -> FiveOf Char -> Bool
matches (State (FiveOf poss1 poss2 poss3 poss4 poss5) reqs) word@(FiveOf c1 c2 c3 c4 c5) =
  Set.member c1 poss1 &&
  Set.member c2 poss2 &&
  Set.member c3 poss3 &&
  Set.member c4 poss4 &&
  Set.member c5 poss5 &&
  Set.isSubsetOf reqs (Set.fromList (toList word))
  --all (`elem` toList word) reqs

main :: IO ()
main = do
  putStrLn "Hello, Haskell!"
